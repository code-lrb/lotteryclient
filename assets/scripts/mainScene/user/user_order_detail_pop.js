/*
订单详情界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {	

        //开奖球
        ballsPrefab:{
            default: [],
            type: cc.Prefab
        },

        orderDtailiItem:{
            default: null,
            type: cc.Prefab
        },

        //订单金额
        labBetAmount:{
            default: null,
            type: cc.Label
        },

        //中奖金额
        labRewardAmount:{
            default: null,
            type: cc.Label
        },
        //共多少单
        labBet:{
            default: null,
            type: cc.Label
        },
        //投注内容
        betNunsContent:{
            default: null,
            type: cc.Layout
        },

        //开奖号码
        ndNumberContent:{
            default: null,
            type: cc.Node
        },

        //订单状态
        labOrderStatus:{
            default: null,
            type: cc.Label
        },

        //中奖显示
        ndRewardIcon:{
            default: null,
            type: cc.Node
        },


        //订单号
        labOrderNum:{
            default: null,
            type: cc.Label
        },

        labOrderTime:{
            default: null,
            type: cc.Label
        },

        //期号彩种名
        labNameIsuse:{
            default: null,
            type: cc.Label
        },

        spLotteryIcon:{
             default: null,
            type: cc.Sprite
        },
        
        //支付	
        quickRechargePagePrefab:{
            default:null,
            type:cc.Prefab
        },

        fbTicketPop:{
            default:null,
            type:cc.Prefab
        },

        fbShare:cc.Prefab,

        _data:null,
        _openBalls:null,//开奖列表
        _chaseNum:0,//追号期数
    },

    // use this for initialization
    onLoad: function () {
        this.initPrefab();
        if(this._data != null)
        {
            this.labOrderNum.string = this._data.OrderNum;//订单号
            this.labBetAmount.string = LotteryUtils.moneytoServer(this._data.Amount);//订单总额
            if(this._data.OrderTime != null)
            {
                var times = Utils.byTimeGetStr(this._data.OrderTime,"-",":");
                this.labOrderTime.string = times;
            }

            if(this._chaseNum>0)
            {
                this.labOrderStatus.string = LotteryUtils.getChaseOrderStatusByStatus(this._data.OrderStatus);
            }
            else
            {
                this.labOrderStatus.string = LotteryUtils.getOrderStatusByStatus(this._data.OrderStatus);
            }
            
            if(this._data.OrderStatus == 14)
            {
                this.ndRewardIcon.active = true;
            }

            this.labRewardAmount.string = (this._data.WinMoney==0 && this._data.OrderStatus == "5"&& this._data.OrderStatus == "2"&& this._data.OrderStatus == "4")?"等待开奖":LotteryUtils.moneytoServer(this._data.WinMoney) + "元";//中奖金额
            this.labNameIsuse.string = LotteryUtils.getLotteryTypeDecByLotteryId(this._data.LotteryCode) + "   第"+this._data.IsuseNum + "期"; 
            this.spLotteryIcon.spriteFrame = CL.MANAGECENTER.getLotteryHotById(this._data.LotteryCode);

            var temp1 = this._data.OpenNumber;//开奖列表
            var temp2 = null;
            try {
                temp2  = eval('(' + this._data.Number + ')');//投注内容
            } catch (error) {
                cc.log("user_oder_detail_pop 1:"+error);
            }
            var bopen = false;
            if(temp1 != null)
            {
                var tempArry = temp1.split(" ");
                bopen = true;
                if(this._data.LotteryCode == "101" || this._data.LotteryCode == "102")
                {
                    var Nums = [];
                    for(var i=0 ;i<tempArry.length;i++)
                    {
                         var value = parseInt(tempArry[i]);
                         Nums.push(CL.MANAGECENTER.getDiceSpriteFrameByNum(value)); 
                    }

                    var ball = cc.instantiate(this._openBalls);
                    ball.getComponent('bet_public_balls').init({
                        spNums: Nums,
                        nums:null,
                    });  
                    this.ndNumberContent.addChild(ball);    
                    ball.x = 321;
                    ball.scaleX = 0.8;
                    ball.scaleY = 0.8;
                }
                else
                {
                    var ball = cc.instantiate(this._openBalls);
                    ball.getComponent('bet_public_balls').init({
                        spNums: null,
                        nums:tempArry,
                    });  
                    ball.x = 321;
                    this.ndNumberContent.addChild(ball);    
                }
            }
            var betNums = 0;
            if(temp2 !=null)
            {
               var numbers =  temp2.length;
               
               for(var i=0;i<numbers;i++)
               {    
                    betNums += parseInt(temp2[i].Data.length);
                    for(var j=0;j<temp2[i].Data.length;j++)
                    {
                        var oldNums = "";
                        var playcode = temp2[i].PlayCode.toString();
                        oldNums = temp2[i].Data[j].Number;
                        var content = LotteryUtils.getPlayTypeDecByPlayIdNor(playcode,temp2[i].Data[j].isNorm) + "  " ;
                        content += temp2[i].Data[j].Bet + "注  ";
                        content += temp2[i].Data[j].Multiple + "倍  ";
                        var amo = 0;
                        if(playcode == "90102")
                           amo= parseInt(temp2[i].Data[j].Bet) * parseInt(temp2[i].Data[j].Multiple) * 3;
                        else
                            amo= parseInt(temp2[i].Data[j].Bet) * parseInt(temp2[i].Data[j].Multiple) * 2;
                        content +=  amo.toString()+ "元";
                        var item = cc.instantiate(this.orderDtailiItem);
                        var nums = LotteryUtils.analyseRewardNumber(oldNums,temp1,playcode,this._data.LotteryCode,true);
                        item.getComponent('user_order_detail_item').init({
                            number:nums,
                            dec:content,
                        });  
                        this.betNunsContent.node.addChild(item);    
                    }
               }
            }
            this.labBet.string = "共" + betNums.toString() + "单";
         }
    },

    initPrefab:function(){
        var ID = this._data.LotteryCode.toString();
        var type = ID.substring(0,1);
        this._openBalls = null;
        var index = 0;
        switch (parseInt(type))
        {
            case 1://k3
            {
               index = 1;
            }
            break;
            case 2://115
            {  
                index = 2;
            }
            break;
            case 3://ssc
            {
                index = 2;
            }
            break;
            case 8://ssq
            {
                index = 4;
            }
            break;
            case 9://big
            {
                index = 5;
            }
            break;    
        }
        this._openBalls = this.ballsPrefab[index];
    },

    init:function(ret,isChase,orderCode,ChaseCode){
        this._data = ret;
        this._chaseNum = isChase;
        this.chaseCode = ChaseCode;
        this.orderCode = orderCode;
    },

    //继续投
    onKeepOn:function(){
        window.Notification.emit("HallUI_toGotoRoom",this._data.LotteryCode);
    },

    //抄单购买
    onMore:function(){
        this.getCurIssue();
    },

    //查看出票
    onTicket:function(){
        var canvas = cc.find("Canvas");
        var ticket = cc.instantiate(this.fbTicketPop);
        ticket.getComponent(ticket.name).init(this.orderCode,this.chaseCode);
        canvas.addChild(ticket);
    },

    //普通订单付款
    basePay:function(){
        var dataRev = {
            buyType:0,
            dataBase:this._data.Number,
            lotteryId:this._data.LotteryCode,
            money:this._data.Amount,
            otherBase:""
        }
         var recv = function(ret){
            ComponentsUtils.unblock();
            var obj = ret.Data;
            if(ret.Code === 0){
                var isuseNum = obj.IsuseNum;      
                var endTimeDate = Utils.getDateForStringDate(obj.EndTime.toString());  
                var currentTimeStamp = parseInt(obj.ServerTime/1000);
                var endTimeStamp = Date.parse(endTimeDate)/1000;
             
                var leftTimeStamp = endTimeStamp - currentTimeStamp;
                if(leftTimeStamp <= 0){
                    cc.log("当前期数已截止");
                    ComponentsUtils.showTips("当前期数已截止");
                    return;
                }
                else
                {
                     var data = {
                         lotteryID:dataRev.lotteryId,    
                         lotteryMoney:dataRev.money,
                         baseData:dataRev.dataBase,
                         otherData:dataRev.otherBase,
                         buyType:dataRev.buyType,
                         isuseid:obj.IsuseNum,
                         endTime:obj.EndTime,
                         beginTime:obj.BeginTime,
                         data:"",
                         gold:0,
                         paymentType:2,
                     }
                    var balace = User.getBalance();
                    var toSerMoney = LotteryUtils.moneytoServer(dataRev.money);
                    if(balace<toSerMoney){
                        var quickPayCallback = function(ret){
                            cc.log("quickPayCallback success");
                            data.data = ret.data;
                            data.gold = ret.gold;
                            data.paymentType = ret.paymentType;
                            this.doBalancePay(data);
                        }.bind(this);
                        var quickRechargePage = cc.instantiate(this.quickRechargePagePrefab);
                        var canvas = cc.find("Canvas");
                        canvas.addChild(quickRechargePage);
                        quickRechargePage.getComponent("lottery_quickRecharge").init(dataRev.lotteryId, obj.IsuseNum,toSerMoney,dataRev.buyType,true,quickPayCallback);
                     }
                     else{
                         this.doBalancePay(data);
                     }
                }
            }else{
                ComponentsUtils.showTips(ret.Msg);
                return;
            }
        }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            LotteryCode:this._data.LotteryCode,
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETRECURRENTISUSE, data, recv.bind(this),"POST");   
        ComponentsUtils.block(); 
    },

    //复制到剪切板
    onCopy:function() {
        if(cc.sys.os == cc.sys.OS_ANDROID || cc.sys.os == cc.sys.OS_IOS)
        {
            //ComponentsUtils.showTips("已复制");
            //PlatformUtils.copyToClipboard(this._data.OrderNum);
        }
        else
        {
            var save = function (e) {
                e.clipboardData.setData('text/plain', this._data.OrderNum);
                e.preventDefault();
            }.bind(this);

            document.addEventListener('copy', save);
            document.execCommand('copy');
            document.removeEventListener('copy', save);
            ComponentsUtils.showTips("已复制订单号到剪切栏");
        }
    },

    getCurIssue:function(){
         var recv = function(ret){
            ComponentsUtils.unblock();
            var obj = ret.Data;
            if(ret.Code === 0){
                var isuseNum = obj.IsuseNum;      
                var endTimeDate = Utils.getDateForStringDate(obj.EndTime.toString());  
                var currentTimeStamp = parseInt(obj.ServerTime/1000);
                var endTimeStamp = Date.parse(endTimeDate)/1000;
             
                var leftTimeStamp = endTimeStamp - currentTimeStamp;
                if(leftTimeStamp <= 0){
                    cc.log("当前期数已截止");
                    var endIsuseStr = obj.IsuseNum + "期已截止";
                    ComponentsUtils.showTips(endIsuseStr);
                    return;
                }
                else
                {
                    var confirmCallback = function(){
                        this.basePay();
                    }.bind(this);
                    var closeCallback = function(){
                    }.bind(this);
                    var strDec = "";
                    strDec = "第"+ obj.IsuseNum + "期支付订单金额"+LotteryUtils.moneytoServer(this._data.Amount)+"元。是否确认支付？";
                    ComponentsUtils.showAlertTips(2, strDec, closeCallback,"抄单购买" , confirmCallback);   
                }
            }else{
                ComponentsUtils.showTips(ret.Msg);
                return;
            }
        }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            LotteryCode:this._data.LotteryCode,
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETRECURRENTISUSE, data, recv.bind(this),"POST");   
        ComponentsUtils.block(); 
    },

    //账户余额支付
    doBalancePay:function(dataBase){
        var betData = "";
        var istr = dataBase.baseData.substr(0, 1);
        if(istr != "[")
        {
            betData = "[" +dataBase.baseData + "]";
        }
        else
        {
            betData = dataBase.baseData;
        }
        
        var recv = function(ret){
            ComponentsUtils.unblock(); 
            if(ret.Code === 0)
            {
                ComponentsUtils.showTips("支付成功");
                window.Notification.emit("USER_useruirefresh","");
                this.onClose();
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            IsuseNum:dataBase.isuseid,
            BeginTime:dataBase.beginTime,
            EndTime:dataBase.endTime,
            RoomCode:Chat.getChatRoomIDByLotteryID(dataBase.lotteryID),
            BetData:betData,
            ChaseData:dataBase.otherData,
            UserCode:User.getUserCode(),
            LotteryCode:dataBase.lotteryID,
            BuyType:0,//0代购 1追号 2跟单
            Amount:dataBase.lotteryMoney,
            Coupons:dataBase.data,
            PaymentType:dataBase.paymentType,//1.余额彩豆购彩券支付 2.余额支付 3.购彩券支付 4.彩豆支付 5.余额和购彩券支付 6.余额和彩豆支付 7.彩豆和购彩券支付
            Gold:dataBase.gold//彩豆支付
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.REQUESTBET, data, recv.bind(this),"POST");      
        ComponentsUtils.block(); 
    },

    //追号订单付款 暂时没有
    chasePay:function(){
    },

    onClose:function(){
        this.node.getComponent("Page").backAndRemove();
    },

    onShare:function(flag){
        ComponentsUtils.showTips("暂时不支持微信分享！");
    }

});

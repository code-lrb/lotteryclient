/**
 * 优惠卷
 */
cc.Class({
    extends: cc.Component,

    properties: {
        fbTicketItem:{
            default: null,
            type: cc.Prefab
        },

        ndContent:{
            default:null,
            type:cc.Node
        },

        ndSufContent:{
            default:null,
            type:cc.Node
        },

        ndFailContent:{
            default:null,
            type:cc.Node
        },

        sussful:[],
        fail:[],

        _orderCode:0,
        _chaseCode:0,
        _pageIndex:1,
        _pageSize:20,
    },

    // use this for initialization
    onLoad: function () {
        this._pageIndex = 1;
        this.ticketInfo();
    },

    init:function(ordercode,chasecode){
        this._orderCode = ordercode;
        this._chaseCode = chasecode;
    },

    ticketInfo:function(){
        var recv = function recv(ret) { 
            ComponentsUtils.unblock();
            if(ret.Code == 0 )
            {
                if(ret.Data != null || ret.Data != "")
                {
                    for(var i=0;i<ret.Data.length;i++)
                    {
                         var ticket = cc.instantiate(this.fbTicketItem);
                         ticket.getComponent(ticket.name).init(ret.Data[i]);
                         this.ndContent.addChild(ticket);
     
                         if(ret.Data[i].TicketStatus == 3 || ret.Data[i].TicketStatus == 4)
                         {
                             var ticket1 = cc.instantiate(this.fbTicketItem);
                             ticket1.getComponent(ticket1.name).init(ret.Data[i]);
                             this.ndFailContent.addChild(ticket1);
                         }
                         else
                         {
                             var ticket2 = cc.instantiate(this.fbTicketItem);
                             ticket2.getComponent(ticket2.name).init(ret.Data[i]);
                             this.ndSufContent.addChild(ticket2);
                         }
                    }
                    this._pageIndex++;
                }
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }

         }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            OrderCode:this._orderCode,
            ChaseCode:this._chaseCode,
            PageIndex:this._pageIndex,
            PageSize:this._pageSize
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETLOOKTICKET, data, recv.bind(this),"POST");    
        ComponentsUtils.block();
    },

      //滚动获取下一页
      scrollCallBack: function (scrollview, eventType, customEventData) {
        if(eventType === cc.ScrollView.EventType.BOUNCE_BOTTOM)
        {
            var offset_y = scrollview.getScrollOffset().y;
            var max_y = scrollview.getMaxScrollOffset().y; 
            if(offset_y - max_y>200){
                this.ticketInfo();
            }
        }
    },

    onClose: function(){
        this.node.getComponent("Page").backAndRemove();
    },

});

/**
 * !#zh 快3走势图号码分布组件   (二同号)
 * @information 开奖号
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labIssue:{
            default:null,
            type:cc.Label
        },

        labOpenNums:{
            default:null,
            type:cc.Label
        },

        labNums:{
            default:[],
            type:cc.Label
        },

        ndNumBg:{
            default:[],
            type:cc.Node
        },

        ndBg:{
            default:null,
            type:cc.Node
        },
        _data:null,
        _ruleDec:[]
    },
    // use this for initialization
    onLoad: function () {
          if(this._data != null ){
            this.ndBg.color = this._data.color;
            this.colorStr = new cc.Color(255, 255, 255);
            this._ruleDec = ["11","22","33","44","55","66"];
            var isuseStr = this._data.Isuse.substring(this._data.Isuse.length-2,this._data.Isuse.length) + "期";
            this.labIssue.string = isuseStr;
            var openStr = "";
            for(var i=0;i<this._data.openNums.length;i++)
            {
                openStr += this._data.openNums[i]+" ";
            }
            this.labOpenNums.string = openStr;

            for(var i=0;i<this._data.nums.length;i++ )
            {
                this.labNums[i].string = this._data.nums[i]==0?this.setSign(i,this._data.nums[i],this.labNums[i]):this._data.nums[i];
            }
         }
    },


    setSign:function(index,num,label){
        label.node.color = this.colorStr;
        this.ndNumBg[index].active = true;
        return this._ruleDec[index];
    },

    /** 
    * 接收走势图号码分布信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }
    
});

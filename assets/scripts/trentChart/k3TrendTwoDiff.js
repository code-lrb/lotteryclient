cc.Class({
    extends: cc.Component,

    properties: {
        pfOpenItem:{
            default:null,
            type:cc.Prefab
        },

        //基本走势普通
        pfBaseItem:{
            default:null,
            type:cc.Prefab
        },

        //走势统计
        pfBase1Item:{
            default:null,
            type:cc.Prefab
        },

        //号码期号
        pfIssueItem:{
            default:null,
            type:cc.Prefab
        },

        //号码期号统计
        pfIssue1Item:{
            default:null,
            type:cc.Prefab
        },

        //号码列表
        pfNumContentItem:{
            default:null,
            type:cc.Prefab
        },

        //冷热
        fbHotItem:{
            default:null,
            type:cc.Prefab
        },

        //开奖界面
        ndOpenPanle:{
            default:null,
            type:cc.Node
        },

        //走势界面
        ndBasePanle:{
            default:null,
            type:cc.Node
        },

        //号码界面
        ndNumsContent:{
            default:[],
            type:cc.Node
        },

        //号码
        swContent:{
            default:[],
            type:cc.ScrollView
        },

        //冷热界面
        ndHotContent:{
            default:null,
            type:cc.Node
        },

        spSort:{
            default:[],
            type:cc.SpriteFrame
        },

        _data:null,
        _hotData:[],
        _openItemArr: [],
        _reaCount: 18,
        _spacing: 4,
        _totalCount: 50,
        _addType: false,
        _addType1: false
    },

    // use this for initialization
    onLoad: function () {
        this.showPage(this._data);
    },

    init:function(data){
        this._data = data;
    },

    showPage:function(info){
        this._color =[];
         var color = [];
        var color1 = new cc.Color(253, 253, 251);
        var color2 = new cc.Color(246, 247, 241);
        color.push(color1);
        color.push(color2);
        this._color.push(color1);
        this._color.push(color2);

        var tempinfo = eval('('+ info +')');
        var trdata = tempinfo["tr"]; 
        
        this._openData = trdata;

        this._totalCount =this._openData.length;
        this.lastPositionY =0;
        this.buffer =730;

        for(var i=0;i<this._reaCount;++i){
            var openItem =cc.instantiate(this.pfOpenItem);
            openItem.setPosition(0,-openItem.height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                form:this._openData[i].x,
                color:this._color[i%2]
            };
            openItem.getComponent(openItem.name).onItemIndex(i);
            openItem.getComponent(openItem.name).init(data); 
            this.ndOpenPanle.addChild(openItem);
            this._openItemArr.push(openItem);
        }
        this.openItemHeight =openItem.height;
        this.ndOpenPanle.height =this._totalCount*(this.openItemHeight+this._spacing)+this._spacing;

        var baseNums = ["12","13","14","15","16","23","24","25","26","34","35","36","45","46","56"];
        //号码走势
        var numContentItem = cc.instantiate(this.pfNumContentItem);
        var data = {
            nums:baseNums,
            color:color[0]
        };
        numContentItem.getComponent(numContentItem.name).init(data);
        this.ndNumsContent[1].addChild(numContentItem);    

        this.baseItemArr =[];
        this.issueItemArr =[];
        this.base1ItemArr =[];
        this.issue1ItemArr =[];
        for(var i=0;i<this._openData.length;i++)
        {
             //基本走势
            var baseItem = cc.instantiate(this.pfBaseItem);
            var data = {
                Isuse:this._openData[i].i,
                num1:this._openData[i].n,
                num2:this._openData[i].s,
                num3:this._openData[i].sp,
                num4:this._openData[i].t,
                color:color[i%2]
            };
            baseItem.getComponent(baseItem.name).init(data);
            this.baseItemArr.push(baseItem);
           
            //号码走势
            var issueItem = cc.instantiate(this.pfIssueItem);
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                color:color[i%2]
            };
            issueItem.getComponent(issueItem.name).init(data);
            this.issueItemArr.push(issueItem);   

            //号码走势
            var numContentItem = cc.instantiate(this.pfNumContentItem);
            var data = {
                nums:this._openData[i].f,
                color:color[i%2],
            };
            numContentItem.getComponent(numContentItem.name).init(data);
            this.ndNumsContent[2].addChild(numContentItem);   
        }

         //统计出现期数、最大、平均遗漏、最大连出
        var censusStr = ["出现次数","平均遗漏","最大遗漏","最大连出"];
        var censtrCor1 = new cc.Color(122,30,150);
        var censtrCor2 = new cc.Color(37,87,0);
        var censtrCor3 = new cc.Color(110,35,0);
        var censtrCor4 = new cc.Color(0,96,132);
        var censtrCor5 = new cc.Color(229,225,214);
        var censtrCor6 = new cc.Color(234,233,229);
        var colorBg = [censtrCor5,censtrCor6];
        var colorStr = [censtrCor1,censtrCor2,censtrCor3,censtrCor4];

        var bnData = tempinfo["bn"];
        var baData = tempinfo["ba"];
        var bmData = tempinfo["bm"];
        var bsData = tempinfo["bs"];
        var bnamsList = [];
        bnamsList.push(bnData);
        bnamsList.push(baData);
        bnamsList.push(bmData);
        bnamsList.push(bsData);

        var snData = tempinfo["sn"];
        var saData = tempinfo["sa"];
        var smData = tempinfo["sm"];
        var ssData = tempinfo["ss"];
        var snamsList = [];
        snamsList.push(snData);
        snamsList.push(saData);
        snamsList.push(smData);
        snamsList.push(ssData);


        for(var i=0;i<4;i++)
        {
            //基本走势统计
            var base1Item = cc.instantiate(this.pfBase1Item);
            var data = {
                color:color[(this._openData.length+i-1)%2],
                dec:censusStr[i],
                decColor:colorStr[i],
                decBgColor:colorBg[i%2],
                nums:bnamsList[i],
                numColor:colorStr[i]
            }
            base1Item.getComponent(base1Item.name).init(data);
            this.base1ItemArr.push(base1Item);
           
            //号码统计
            var issue1Item = cc.instantiate(this.pfIssue1Item);
            issue1Item.getChildByName("spBg").color = colorBg[i%2];
            issue1Item.getChildByName("labDec").color = colorStr[i];
            issue1Item.getChildByName("labDec").getComponent(cc.Label).string = censusStr[i];
            this.issue1ItemArr.push(issue1Item);
        }

        //冷热
        var oData = tempinfo["o"];
        var o3Data = tempinfo["o3"];
        var o5Data = tempinfo["o5"];
        var o0Data = tempinfo["o0"];
        this._hotData= [];
        for(var i=1;i<7;i++)
        {
            //冷热
            var data = {
                type:3,
                num:i,
                issue30:o3Data[i-1],
                issue50:o5Data[i-1],
                issue0:o0Data[i-1],
                miss:oData[i-1],
                color:color[i%2]
            }
  
            this._hotData.push(data);
            var hotItem = cc.instantiate(this.fbHotItem);
            hotItem.getComponent(hotItem.name).init(data);
            this.ndHotContent.addChild(hotItem);
        }
    },

    //开奖期号排序
    issueSort:function(toggle){
        this.ndOpenPanle.parent.parent.getComponent(cc.ScrollView).scrollToTop(0.1);
        if(toggle.getComponent(cc.Toggle).isChecked){
            this._openData =this._openData.reverse(); 
        }
        else{
            this._openData =this._openData.reverse();
        }
       
        for(var i=0;i<this._openItemArr.length;++i){
            this._openItemArr[i].setPosition(0,- this._openItemArr[i].height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                form:this._openData[i].x,
                color:this._color[i%2]
            };
            this._openItemArr[i].getComponent(this._openItemArr[i].name).onItemIndex(i);
            this._openItemArr[i].getComponent(this._openItemArr[i].name).updateData(data); 
        }
    },

    //冷热排序
    onAfterSort:function(toggle,customEventData){
        this.hotSort(toggle,this.ndHotContent,this._hotData,customEventData);
    },

    hotSort:function(toggle,panle,arry,key){
        if(arry == null)
            return;
        toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[0];
        if(this._frontTg!=null)
            this._frontTg.target.getComponent(cc.Sprite).spriteFrame = this.spSort[0];
        this._frontTg = toggle;

        var children = panle.children;
        var childrenLen = children.length;
        
        var keyStr = "";
        switch (key)
        {
            case "1"://号码
            {
                if(toggle.getComponent(cc.Toggle).isChecked)    
                {
                    Utils.sortByKey(arry,"num",true);
                    if(childrenLen == arry.length)
                    {
                        for (var i = 0; i < childrenLen; ++i) {
                            children[i].getComponent(children[i].name).updateData(arry[i]);
                        }
                    }
                }
                else
                {
                    Utils.sortByKey(arry,"num",false);
                    if(childrenLen == arry.length)
                    {
                        for (var i = 0; i < childrenLen; ++i) {
                            children[i].getComponent(children[i].name).updateData(arry[i]);
                        }
                    }
                    toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[2];
                }
                return;
            }
            break;
            case "2"://30期
            {
                keyStr = "issue30";
            }
            break;
            case "3"://50期
            {
                keyStr = "issue50";
            }
            break;
            case "4"://100期
            {
                keyStr = "issue0";
            }
            break;
            case "5"://遗漏
            {
                keyStr = "miss";
            }
            break;
            default:
            break;
        }

        if(keyStr != "")
        {
            if(toggle.getComponent(cc.Toggle).isChecked)    
            {
                Utils.sortByKey(arry,keyStr,false);
                if(childrenLen == arry.length)
                {
                    for (var i = 0; i < childrenLen; ++i) {
                        children[i].getComponent(children[i].name).updateData(arry[i]);
                    }
                }
                
            }
            else
            {
                Utils.sortByKey(arry,keyStr,true);
                if(childrenLen == arry.length)
                {
                    for (var i = 0; i < childrenLen; ++i) {
                        children[i].getComponent(children[i].name).updateData(arry[i]);
                    }
                }
                toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[1];
            }
        } 
    },

    onScrollowPanle1:function(scrollview, eventType){
      //  cc.log("scrol-0");
        var offset = scrollview.getScrollOffset();
        this.swContent[2].scrollToOffset(offset);
    },

    onScrollowPanle2:function(scrollview, eventType){
     //   cc.log("scrol-1");
        var offset = scrollview.getScrollOffset();
        var offx = Math.abs(offset.x);
        this.swContent[2].scrollToOffset(cc.p(offx,offset.y));
    },

    onScrollowPanle3:function(scrollview, eventType){
     //   cc.log("scrol-2");
        var offset = scrollview.getScrollOffset();
        var offx = 0 - offset.x;
        this.swContent[0].scrollToOffset(offset);
        this.swContent[1].scrollToOffset(cc.p(offx,offset.y));
    },

    onClose:function(){
         
    },

    scrollCallBackFun: function(){
        if(!this._openItemArr.length){
            return;
        }else{
            Utils.preNodeComplex(this.ndOpenPanle,this.openItemHeight,this._spacing,this._reaCount,this._openItemArr,this.buffer,this._openData,this._color,'k3_opencontent_item'); 
           
        }
    },

    //----切换到那个单选按钮，才将它们addChild到舞台上来
    //基本走势
    tgBaseTrend: function(){
        if(this._addType){return}
        for(var i=0;i<this.baseItemArr.length;++i){
            this.ndBasePanle.addChild(this.baseItemArr[i]);  
            this._addType =true; 
        }
        for(var i=0;i<this.base1ItemArr.length;++i){
            this.ndBasePanle.addChild(this.base1ItemArr[i]);
            this._addType =true;    
        }    
    },
    //形态走势
    tgNumsTrend: function(){
        if(this._addType1){return}
        for(var i=0;i<this.issueItemArr.length;++i){
            this.ndFormContent.addChild(this.formItemArr[i]);  
            this._addType1 =true; 
        }
        for(var i=0;i<this.issue1ItemArr.length;++i){
            this.ndFormContent.addChild(this.form1ItemArr[i]);
            this._addType1 =true;    
        }    
    }

});

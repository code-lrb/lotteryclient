(function(){ 
    //房间id号 环信
    var _chatRommID = {
        "301":"",
        "101":"",
        "102":"",
        "801":"",
        "901":"",
        "201":"",
        "202":"",
    };

    function _getChatRoomIDByLotteryID(lotteryid) {
        return  _chatRommID[lotteryid];
    }
    
    function _setChatRoomIDByLotteryID(lotteryid,chatroomid) {
        _chatRommID[lotteryid] = chatroomid;
    }

    var _Chat={
        getChatRoomIDByLotteryID:_getChatRoomIDByLotteryID,
        setChatRoomIDByLotteryID:_setChatRoomIDByLotteryID
    }

    window.Chat=_Chat; 
} 
)();